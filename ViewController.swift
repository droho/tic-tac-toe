//
//  ViewController.swift
//  tic-tac-toe
//
//  Created by Damian Drohobycki on 21/05/2019.
//  Copyright © 2019 Damian Drohobycki. All rights reserved.
//

import UIKit

typealias Move = Int
enum Piece: String {
    case X = "X"
    case O = "O"
    case E = " "
    var opposite: Piece {
        switch self {
        case .X:
            return .O
        case .O:
            return .X
        case .E:
            return .E
        }
    }
}

class ViewController: UIViewController {

    private var game = Game()
    private let computer = Computer()
    
    var round: Int = 0
    var playerPoints: Int = 0
    var drawPoints: Int = 0
    var computerPoints: Int = 0
    
    @IBOutlet var fields: [UIButton]!
    @IBOutlet weak var playerScore: UILabel!
    @IBOutlet weak var drawScore: UILabel!
    @IBOutlet weak var computerScore: UILabel!
    @IBOutlet weak var actualMove: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        start()
    }

    
    @IBAction func userAction(_ sender: UIButton) {
        actualMove.text = "computer"
        user(move: sender.tag)
        for field in fields where game.legalMoves.contains(field.tag) {
            field.isEnabled = false
        }
        computer.findBestMove(game) {
            self.show(move: $0)
            self.game = self.game.move($0)
            self.actualMove.text = "player"
            for field in self.fields where self.game.legalMoves.contains(field.tag) {
                field.isEnabled = true
            }
            guard self.game.isWin || self.game.isDraw else { return }
            self.restartGame()
        }
        guard game.isWin || game.isDraw else { return }
        restartGame()
    }
    
    
    private func user(move: Move) {
        show(move: move)
        game = game.move(move)
        guard game.isWin || game.isDraw else { return }
        restartGame()
    }
    
    private func show(move: Move) {
        guard let field = fields.first(where: { $0.tag == move }) else { return }
        field.setTitle(game.turn.rawValue, for: .normal)
        field.isEnabled = false
    }
    
    private func start() {
        clear()
        round += 1
    }
    
    private func clear() {
        fields.forEach {
            $0.setTitle("", for: .normal)
            $0.isEnabled = true
        }
    }
    
    private func restartGame() {
        if game.isDraw {
            drawPoints += 1
            drawScore.text = "\(drawPoints)"
        }
        if game.isWin {
            if round % 2 == 0 {
                playerPoints += 1
                playerScore.text = "\(playerPoints)"
            } else {
                computerPoints += 1
                computerScore.text = "\(computerPoints)"
            }
        }
        
        clear()
        game = Game()
    }
    
}

