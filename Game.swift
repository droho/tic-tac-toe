//
//  Board.swift
//  tic-tac-toe
//
//  Created by Damian Drohobycki on 21/05/2019.
//  Copyright © 2019 Damian Drohobycki. All rights reserved.
//

import Foundation

struct Game {
    let position: [Piece]
    let turn: Piece
    let lastMove: Move
    
    init(position: [Piece] = [.E, .E, .E, .E, .E, .E, .E, .E, .E], turn: Piece = .X, lastMove: Int = -1) {
        self.position = position
        self.turn = turn
        self.lastMove = lastMove
    }
    
    func move(_ location: Move) -> Game {
        var tempPosition = position
        tempPosition[location] = turn
        return Game(position: tempPosition, turn: turn.opposite, lastMove: location)
    }
    
    var legalMoves: [Move] {
        return position.indices.filter { position[$0] == .E }
    }
    
    var isWin: Bool {
        return
            position[0] == position[1] && position[0] == position[2] && position[0] != .E ||
                position[3] == position[4] && position[3] == position[5] && position[3] != .E ||
                position[6] == position[7] && position[6] == position[8] && position[6] != .E ||
                position[0] == position[3] && position[0] == position[6] && position[0] != .E ||
                position[1] == position[4] && position[1] == position[7] && position[1] != .E ||
                position[2] == position[5] && position[2] == position[8] && position[2] != .E ||
                position[0] == position[4] && position[0] == position[8] && position[0] != .E ||
                position[2] == position[4] && position[2] == position[6] && position[2] != .E 
        
    }
    
    var isDraw: Bool {
        return !isWin && legalMoves.count == 0
    }
    
}
