//
//  Computer.swift
//  tic-tac-toe
//
//  Created by Damian Drohobycki on 21/05/2019.
//  Copyright © 2019 Damian Drohobycki. All rights reserved.
//

import Foundation

class Computer {
    
    func minimax(_ game: Game, maximizing: Bool, originalPlayer: Piece) -> Int {
        if game.isWin && originalPlayer == game.turn.opposite { return 1 }
        else if game.isWin && originalPlayer != game.turn.opposite { return -1 }
        else if game.isDraw { return 0 }
        
        if maximizing {
            var bestEval = Int.min
            for move in game.legalMoves {
                let result = minimax(game.move(move), maximizing: false, originalPlayer: originalPlayer)
                bestEval = max(result, bestEval)
            }
            return bestEval
        } else {
            var worstEval = Int.max
            for move in game.legalMoves {
                let result = minimax(game.move(move), maximizing: true, originalPlayer: originalPlayer)
                worstEval = min(result, worstEval)
            }
            return worstEval
        }
    }
    
    func findBestMove(_ game: Game, completionHandler: ((Move) -> Void)?) {
        var bestEval = Int.min
        var bestMove = -1
        DispatchQueue.main.async {
            for move in game.legalMoves {
                let result = self.minimax(game.move(move), maximizing: false, originalPlayer: game.turn)
                if result > bestEval {
                    bestEval = result
                    bestMove = move
                }
            }
            completionHandler?(bestMove)
        }
    }
    
}
